## rename_subtitles

rename_subtitles is a shell script for renaming the subtitles with the same name of the series/movies.
Just place and run the script on the folder with the files that we want to rename.

---

## Why this?

I create this script with the purpose to help me renaming the subtitiles. 

I like watch series and movies and in case of series i like to pull all the complete seasons, and because of that i have a annoying problem I need to rename all the subtitles with the same name of the video for the player read them.

Like you have imagine if I have more than 1 Season I will spend some time doing that, our if in one season we have 24 episodes!

Sure they already exists some software that do that, but i need to download install and they have to mutch options that i don't need.

So, in a simple way i create this script, that do the job quick and easy way.

---

## Install

Just copy the file to your folder and runnit.
I recommend using like me, place it on the your /bin folder.

### Example: 

```sh
$ sudo cp rename_subtitles.sh /bin/
```

---

## Usage

Call the file rename_subtitles.sh if the copy was made to **/bin** our **./rename_subtitles.sh** if is on the same directory of the target folder.

### Example file in /bin
```sh
$ /mnt/d/krypton$ rename_subtitles.sh
Renaming the subtitles of the Serie.
```

### Example file in current directory
```sh
$ /mnt/d/krypton$ ./rename_subtitles.sh
Renaming the subtitles of the Serie.
``` 

---

## Version

### v2.0 
  - Added support for renaming movies.
### v1.4
  - Fix whitespaces.
### v1.3
  - Trim whitespaces.
### v1.2 
  - Added support for new files avi, mpg
### V1.1 
  - Added support to detect automatically the season and episode for mkv files.
  - ~~Hardcoded string for renaming the files.~~
### V1.0 
  - Renaming *.srt subtitles for Series.
  - Manually passing the type of name for the filename to be detected.

---

## License

This project is licensed under the MIT License. 