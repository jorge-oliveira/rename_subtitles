#!/bin/bash
#
# Script for renaming the subtitles with the same name of the series/movies.
# Just run the script on the folder with the files that we want to rename.
# Create date: 2018/06/25
# Version: V2.0
# Usage: ./rename_subtitles.sh
# Revision Date: 2019/03/20

# types of the extension to search
ARRAY_DISK_Quantity=(srt mkv avi mpg)

# regex to find
SRT_REGEX='(.*)([sS][0-9][0-9][eE][0-9][0-9])(.*.[Ss][Rr][tT])'
MOV_REGEX='(.*)([sS][0-9][0-9][eE][0-9][0-9])(.*)'


# Convert all the files to upper
function upperCase(){

	for i in ls *.*
	do
		# echo $i
		# echo $CURRENT_FILE
		if [[ -f "$i" && ! "$i" == "$CURRENT_FILE" ]]
		then
			# echo $i
			# rename all the files to uppercase and move the working_folder

			filename_no_whitespace="$(echo -e "${i}" | tr -d '[:space:]')" # remove whitespace
            filename="${filename_no_whitespace%.*}" # get only the filename without extension
            extension="${filename_no_whitespace##*.}" # get only the extension
			upper="${filename^^}" # convert the filename to upper
			complete="${upper}.${extension}" # Join the upper filename and the extension

            # echo "filename_no_whitespace: "$filename_no_whitespace
			# echo "filename: "$filename
			# echo "extension: "$extension
			# echo "upper: "$upper
			# echo "complete: "$complete

			# rename all the files that exists only the ARRAY_DISK_Quantity
            for file in "${ARRAY_DISK_Quantity[@]}"
                do
                    if [[ "$file" == "$extension" ]] ; then
                        ## rename all the files
                        mv "$i" "$complete" 2> /dev/null # if error exists throw to null
                    fi
            done

		fi
	done

}

# Rename the subtitle when is a movie
function renameMovie(){

	for subtitle in *.srt
	do
		for movie in $(ls *.mkv *.avi *.mpg)
		do
			mv "${subtitle}" "${movie%.*}.srt"
			break
		done
	done

	exit 0

}

# Rename the subtitle when is a serie
function renameSubtitle(){

    ## read all the files in the directory searching for all *.srt files
	for sub in *.srt
	do

	    # the matched part of the string is stored in the BASH_REMATCH array
		if [[ $sub =~ $SRT_REGEX ]] ; then
			# echo The regex matches!
			# echo
            # echo "Array: ${BASH_REMATCH[*]}"
			# echo
			# echo "Array 0 srt: ${BASH_REMATCH[0]}"
            # echo "Array 1 srt: ${BASH_REMATCH[1]}"
            # echo "Array 2 srt: ${BASH_REMATCH[2]}"
			# echo "Array 3 srt: ${BASH_REMATCH[3]}"

			SUB_ARRAY0="${BASH_REMATCH[0]}"
			SUB_ARRAY1="${BASH_REMATCH[1]}"
			SUB_ARRAY2="${BASH_REMATCH[2]}"
			SUB_ARRAY3="${BASH_REMATCH[3]}"
		fi

        ## inner loop for movies found will check if exists a same season and episode matching
		# for mov in *.mkv *.avi *.mpg
		for mov in $(ls *.mkv *.avi *.mpg 2>/dev/null)
		do
	        # the matched part of the string is stored in the BASH_REMATCH array
			if [[ $mov =~ $MOV_REGEX ]]
			then
				# echo The regex matches!
				# echo
				# echo "Array: ${BASH_REMATCH[*]}"
				# echo
				# echo "Array 0 mov: ${BASH_REMATCH[0]}"
				# echo "Array 1 mov: ${BASH_REMATCH[1]}"
                # echo "Array 2 mov: ${BASH_REMATCH[2]}"
				# echo "Array 3 mov: ${BASH_REMATCH[3]}"

				MOV_ARRAY0=${BASH_REMATCH[0]}
				MOV_ARRAY1=${BASH_REMATCH[1]}
				MOV_ARRAY2=${BASH_REMATCH[2]}
				MOV_ARRAY3=${BASH_REMATCH[3]}
			fi

			# echo "####### Starting Verification matching #######"
			if [[ "$SUB_ARRAY2" == "$MOV_ARRAY2" ]]
			then
                # echo "Match Found!"
                # echo "sub: ${SUB_ARRAY2} mov: ${MOV_ARRAY2}"
#                echo "Renaming: $sub -> ${mov%.*}.srt"
                # cp "$mov" "$mov"
                mv "$sub" "${mov%.*}.srt" 2>/dev/null
			fi

		done
	done

	exit 0
}

# identifies the type
function checking_type(){

	for checking in *.srt
	do
		if [[ $checking =~ $srt_REGEX ]] ; then
			echo "Renaming the subtitles of the Serie."
			renameSubtitle
			break
		else
			echo "Renaming the subtitles of the Movie."
			renameMovie
			break
		fi
	done

}

# function to uppercase all characters
upperCase

# check if is a series our a movie to rename the subtitle
checking_type
